import axios from "axios";

const instance = axios.create({
    timeout:3000
})
// 添加请求拦截器
instance.interceptors.request.use(function (config) {
    // 在发送请求之前做些什么
    return config;
  }, function (error) {
    // 对请求错误做些什么
    return Promise.reject(error);
  });

// 添加响应拦截器
instance.interceptors.response.use(function (response) {
    // 对响应数据做点什么
    return response;
}, function (error) {
    // 对响应错误做点什么
    let status = error.response.status

    switch (status) {
        case "401":
            console.log("没有权限");
            break;
        case "404":
            console.log("地址错误");
            break;
        case "500":
            console.log("服务器错误");
            break;
        case "304":
            console.log("重定向");
            break;
        case "200":
            console.log("成功");
            break;
        default:
            break;
    }
    return Promise.reject(error);
});

export default instance