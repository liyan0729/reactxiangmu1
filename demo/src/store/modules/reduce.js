const initialState = {}

export default (state = initialState, { type, payload }) => {
    const newstate = JSON.parse(JSON.stringify(state))
    switch (type) {

  case "first":
    return newstate

  default:
    return state
  }
}
