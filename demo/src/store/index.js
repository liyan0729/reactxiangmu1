import {legacy_createStore,applyMiddleware,combineReducers} from 'redux'

import logger from 'redux-logger'

import thunk from 'redux-thunk'

import reduce from './modules/reduce'

const ALL = combineReducers({
    reduce
})

export default legacy_createStore(ALL, applyMiddleware(thunk, logger))