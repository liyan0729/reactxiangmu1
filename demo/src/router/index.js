import { lazy } from "react";

const router=[
        {
            path: "/",
            redirect: "/home"
        },
    {
        path: "/home",
        component: lazy(() => import("../views/Home")),
        children:[
            
        ]
    }
]

export default router