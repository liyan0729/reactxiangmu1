import React, { Suspense } from 'react'
import { BrowserRouter, Routes, Route, Navigate } from 'react-router-dom'
import routes from './index'
function routerconfig() {
    const renderrouter = (routes) => {
        return routes.map((item, index) => {
            return <Route
                key={index}
                path={item.path}
                element={
                    item.redirect ? <Navigate to={item.redirect}></Navigate> : <item.component />
                }
            >
                {
                    item.children && renderrouter(item.children)
                }
            </Route>
        })
    }
    return (
        <Suspense>
            <BrowserRouter>
                <Routes>
                    {
                        renderrouter(routes)
                    }
                </Routes>
            </BrowserRouter>
        </Suspense>
    )
}

export default routerconfig